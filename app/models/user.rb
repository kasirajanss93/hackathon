class User < ActiveRecord::Base
  before_save { self.email = email.downcase }
  validates :name,  presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },uniqueness: true
  validates :user_name, presence: true,uniqueness: true
  validates :password, presence: true
  validates :phone_no, presence: true,uniqueness: true
  has_secure_password
end