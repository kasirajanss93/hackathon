class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :user_name
      t.decimal :phone_no
      t.string :email
      t.string :area

      t.timestamps null: false
    end
  end
end
